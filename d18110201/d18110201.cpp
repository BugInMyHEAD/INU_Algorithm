#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <iterator>
#include <iomanip>
#include <type_traits>


#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )


using namespace std;


// 비교 결과를 flag로 저장하려 함
using cmp_res_t = int;
constexpr cmp_res_t CMP_RES_T_NULL = 0x00;
constexpr cmp_res_t CMP_RES_T_ET = 0x01;
constexpr cmp_res_t CMP_RES_T_LT = 0x02;
constexpr cmp_res_t CMP_RES_T_GT = 0x04;
constexpr cmp_res_t CMP_RES_T_MASK = CMP_RES_T_ET | CMP_RES_T_LT | CMP_RES_T_GT;
constexpr cmp_res_t CMP_RES_T_LE = CMP_RES_T_LT | CMP_RES_T_ET;
constexpr cmp_res_t CMP_RES_T_GE = CMP_RES_T_GT | CMP_RES_T_ET;
constexpr cmp_res_t CMP_RES_T_NE = CMP_RES_T_LT | CMP_RES_T_GT;
constexpr cmp_res_t CMP_RES_T_ANY = CMP_RES_T_ET | CMP_RES_T_LT | CMP_RES_T_GT;


template<typename T, typename ComparerType>
bool isSorted(
	const T* base,
	size_t numOfElem,
	const ComparerType& lessThan)
{
	for (size_t i1 = 1; i1 < numOfElem; ++i1)
	{
		if (lessThan(base[i1], base[i1 - 1]))
		{
			return false;
		}
	}

	return true;
}


template<typename T>
bool isSorted(const T* base, size_t numOfElem)
{
	return isSorted(base, numOfElem, less<>());
}


template<typename T, typename ComparerType = less<>>
void cocktailShakerSort(
	T* base,
	size_t numOfElem,
	const ComparerType& lessThan = less<>())
{
	// 진행 방향
	int direction = +1;
	for (
		// left-1과 right 사이가 정렬이 되어있지 않은 부분이다
		size_t left = 1, right = numOfElem - 1;
		left <= right;
		// 1번의 run이 끝날 때마다 진행 방향을 바꿈
		direction = -direction) 
	{
		for (size_t i3 = direction > 0 ? left : right;
			left <= i3 && i3 <= right;
			i3 += direction)
		{
			if (lessThan(base[i3], base[i3 - 1]))
			{
				swap(base[i3 - 1], base[i3]);
			}
		}
		// 1번의 run이 끝났을 때의 i3 위치는 정렬이 된 곳이다
		direction > 0 ? --right : ++left;
	}
}


template<typename T, typename ComparerType = less<>, typename PIType>
static void abstractMergeSort(
	T* base,
	size_t numOfElem,
	const ComparerType& lessThan,
	const PIType& partitionInitializer)
{
	if (numOfElem <= 1)
	{
		return;
	}

	// 분할 구간을 지정하기 위해 할당
	// 이 배열의 사용 시 인덱스 리스트의 끝에는 두 개의
	// numOfElem이 들어가 있어야 한다
	size_t* partition = new size_t[numOfElem + 2];
	// 합병 시 필요한 배열
	T* spare = new T[numOfElem];

	// 분할 과정
	partition[0] = 0;
	partitionInitializer(partition);

	// 합병 과정
	// index[0]에는 0이 들어있으므로 ( index[1] >= numOfElem )이면
	// 하나의 구간만 존재한다는 것이므로 모두 합병된 것이다
	while (partition[1] < numOfElem)
	{
		for (size_t partitionRead = 1, partitionWrite = 1, spareIdx = 0;
			;
			// 인덱스 리스트의 끝 두 요소가 항상 numOfElem이어야 하는 이유는
			// i3가 2씩 증가하기 때문이다
			partitionRead += 2, ++partitionWrite)
		{
			if (partition[partitionRead - 1] >= numOfElem)
			{
				// 인덱스 리스트의 끝 두 요소가 항상 numOfElem이 되도록
				partition[partitionWrite] = numOfElem;
				break;
			}

			size_t leftPartIdx = partition[partitionRead - 1],
				rightPartIdx = partition[partitionRead];
			while (true)
			{
				// 한 쪽 구간의 요소가 모두 옮겨졌으면
				// 다른 쪽 구간의 남은 요소들을 비교 없이 모두 옮긴다
				if (leftPartIdx >= partition[partitionRead])
				{
					while (rightPartIdx < partition[partitionRead + 1])
					{
						spare[spareIdx++] = base[rightPartIdx++];
					}
					break;
				}
				if (rightPartIdx >= partition[partitionRead + 1])
				{
					while (leftPartIdx < partition[partitionRead])
					{
						spare[spareIdx++] = base[leftPartIdx++];
					}
					break;
				}

				// base[rightPartIdx] == base[leftPartIdx] 이면
				// 왼쪽 구간 요소가 먼저 복사됨
				spare[spareIdx++] =
					lessThan(base[rightPartIdx], base[leftPartIdx]) ?
						base[rightPartIdx++] : base[leftPartIdx++];
			}

			// 합병되었으므로 두 구간을 구별하는 인덱스 i3를 지운다
			// 이 과정으로 인해 구간의 수가 반으로 준다
			// partition[partitionRead - 1]이 numOfElem이 아니고,
			// partition[partitionRead + 1]이 numOfElem인 두 가지 경우 모두 
			// partition[partitionWrite]에 numOfElem을 대입한다
			// (인덱스 리스트의 끝 두 개는 numOfElem이므로)
			partition[partitionWrite] = partition[partitionRead + 1];
		}

		// 원래의 배열로 복사
		memcpy(base, spare, sizeof(*base) * numOfElem);
	}

	delete[] partition, delete[] spare;
}


template<typename T, typename ComparerType = less<>>
void mergeSort(
	T* base,
	size_t numOfElem,
	const ComparerType& lessThan = less<>())
{
	auto partitionInitializer = [=](size_t* partition)
	{
		for (size_t partitionIdx = 1;
			partitionIdx <= numOfElem;
			++partitionIdx)
		{
			partition[partitionIdx] = partitionIdx;
		}
		partition[numOfElem + 1] = numOfElem;
	};
	abstractMergeSort(
		base,
		numOfElem,
		lessThan,
		partitionInitializer);
}


template<typename T, typename ComparerType = less<>>
void naturalMergeSort(
	T* base,
	size_t numOfElem,
	const ComparerType& lessThan = less<>())
{
	auto partitionInitializer = [=](size_t* partition)
	{
		for (size_t partitionIdx = 1, baseIdx = 0;
			baseIdx < numOfElem;
			++partitionIdx)
		{
			cmp_res_t desirableCompareResult = CMP_RES_T_NULL;
			while (true)
			{
				if (++baseIdx >= numOfElem)
				{
					partition[partitionIdx + 1] =
						partition[partitionIdx] =
						numOfElem;
					break;
				}

				cmp_res_t compareResult =
					lessThan(base[baseIdx], base[baseIdx - 1]) ?
						CMP_RES_T_GT : CMP_RES_T_LE;
				if (desirableCompareResult == CMP_RES_T_NULL)
				{
					desirableCompareResult = compareResult;
				}
				// 오름(또는 같음)/내림차순에 변화가 발생하는지 확인한다
				// 두 변수 모두 같은 차순을 의미하는 플래그였다면
				// bitxor에 의해 평가식은 0;
				// 두 변수가 각각 오름차순과 동일을 의미하는 플래그였다면
				// bitand에 의해 평가식은 0
				else if ((desirableCompareResult ^ compareResult) & CMP_RES_T_GT)
				{
					partition[partitionIdx] = baseIdx;
					break;
				}
			}
			// 내림차순이었다면 오름차순으로 바꾼다
			if (desirableCompareResult & CMP_RES_T_GT)
			{
				for (size_t i5 = partition[partitionIdx - 1],
					j5 = partition[partitionIdx] - 1;
					i5 < j5;
					++i5, --j5)
				{
					swap(base[i5], base[j5]);
				}
			}
		}
	};
	abstractMergeSort(
		base,
		numOfElem,
		lessThan,
		partitionInitializer);
}


template<typename T, typename ComparerType = less<>>
void insertionSort(
	T* base,
	size_t numOfElem,
	const ComparerType& lessThan = less<>())
{
	for (size_t i1 = 1; i1 < numOfElem; ++i1)
	{
		size_t i2;
		for (i2 = i1 - 1; i2 < numOfElem && lessThan(base[i1], base[i2]); --i2) ;
		++i2;
		if (i1 != i2)
		{
			T buffer(move(base[i1]));
			//memmove(&base[i2 + 1], &base[i2], sizeof(T) * (i1 - i2));
			move(&base[i2], &base[i1], &base[i2 + 1]);
			base[i2] = move(buffer);
		}
	}
}


template<typename T, typename ComparerType = less<>>
static T* findPivot(T* base, size_t numOfElem, const ComparerType& lessThan)
{
	if (numOfElem == 0)
	{
		return nullptr;
	}

	T& left = base[0];
	T& mid = base[numOfElem / 2];
	T& right = base[numOfElem - 1];

	if (lessThan(left, mid))
	{
		// left < mid < right
		if (lessThan(mid, right)) return &mid;
		// left < right <= mid
		else if (lessThan(left, right)) return &right;
		// right <= left <= mid
		else return &left;
	}
	else
	{
		// right < mid <= left
		if (lessThan(right, mid)) return &mid;
		// mid <= right < left
		else if (lessThan(right, left)) return &right;
		// mid <= left <= right
		else return &left;
	}
}


template<typename T, typename ComparerType = less<>>
static T* quickSortPartition(
	T* base, size_t numOfElem, T* pivot, const ComparerType& lessThan)
{
	T* left = base;
	T* right = base + numOfElem - 1;
	T* small = left;
	T* big = right;

	//Swap(numOfElem, left, findPivot(numOfElem, left, right, lessThan));

#define PARTITION_ALG(ADDRCHK_SMALL, ADDRCHK_BIG)\
do\
{\
	while(1)\
	{\
		while(!lessThan(*pivot, *small))\
		{\
			++small;\
			if(big < small)\
			{\
				goto FuncTerminal;\
			}\
			if(ADDRCHK_SMALL && small < left)\
			{\
				goto FuncTerminal_big;\
			}\
		}\
		while(!lessThan(*big, *pivot))\
		{\
			--big;\
			if(big < small)\
			{\
				goto FuncTerminal;\
			}\
			if(ADDRCHK_BIG && right < big)\
			{\
				goto FuncTerminal_small;\
			}\
		}\
		swap(*small++, *big--);\
		if(big < small)\
		{\
			goto FuncTerminal;\
		}\
	}\
} while(0)

	switch ((right + 1 < left) << 1 | (right < left - 1))
	{
	case 0 : PARTITION_ALG(0, 0); break;
	case 1 : PARTITION_ALG(0, 1); break;
	case 2 : PARTITION_ALG(1, 0); break;
	case 3 : PARTITION_ALG(1, 1); break;
	}

#undef PARTITION_ALG

FuncTerminal:;
	if (pivot >= small)
	{
FuncTerminal_small:;
		swap(*pivot, *small);

		return small;
	}
	else
	{
FuncTerminal_big:;
		swap(*pivot, *big);

		return big;
	}
}


template<typename T, typename ComparerType = less<>>
void quickSortPartialFileExperiment(
	T* base,
	size_t numOfElem,
	size_t smallNumOfElem,
	const ComparerType& lessThan = less<>())
{
	if (numOfElem <= 1)
	{
		return;
	}

	// 둘로 나눠서
	T* xPivot;
	// 중간값 분할도 쓸 것인지 결정(주석 처리)
	//xPivot = base + numOfElem - 1;
	xPivot = findPivot(base, numOfElem, lessThan);
	xPivot = quickSortPartition(base, numOfElem, xPivot, lessThan);

	size_t partNumOfElem;

	// 왼쪽 구역을 정렬
	partNumOfElem = xPivot - base;
	if (partNumOfElem < smallNumOfElem)
	{
		insertionSort(base, partNumOfElem, lessThan);
	}
	else
	{
		quickSortPartialFileExperiment(base, partNumOfElem, smallNumOfElem, lessThan);
	}

	// 오른쪽 구역을 정렬
	partNumOfElem = base + numOfElem - (xPivot + 1);
	if (partNumOfElem < smallNumOfElem)
	{
		insertionSort(xPivot + 1, partNumOfElem, lessThan);
	}
	else
	{
		quickSortPartialFileExperiment(xPivot + 1, partNumOfElem, smallNumOfElem, lessThan);
	}
}


//template<typename T, typename ComparerType = less<>>
//void quickSortLastElemPivot(
//	T* base,
//	size_t numOfElem,
//	const ComparerType& lessThan = less<>())
//{
//	quickSortPartialFileExperiment(base, numOfElem, 0, lessThan);
//}


template<typename T, typename ComparerType = less<>>
void quickSort(
	T* base,
	size_t numOfElem,
	const ComparerType& lessThan = less<>())
{
	quickSortPartialFileExperiment(base, numOfElem, 156, lessThan);
}


//template<typename T, typename ComparerType = less<>>
//void quickSortMedianPivot(
//	T* base,
//	size_t numOfElem,
//	const ComparerType& lessThan = less<>())
//{
//	if (numOfElem <= 1)
//	{
//		return;
//	}
//
//	// 둘로 나눠서
//	T* xPivot;
//	xPivot = findPivot(base, numOfElem, lessThan);
//	xPivot = quickSortPartition(base, numOfElem, xPivot, lessThan);
//	// 왼쪽 구역을 정렬
//	quickSortMedianPivot(
//		base, xPivot - base, lessThan);
//	// 오른쪽 구역을 정렬
//	quickSortMedianPivot(
//		xPivot + 1, base + numOfElem - ( xPivot + 1 ), lessThan);
//}


template<typename ComparerType = less<>, bool minHeap = false, size_t numChild = 2>
class Heapifier
{
	const ComparerType* lessThan = nullptr;


public :

	Heapifier(const ComparerType& lessThan)
	{
		this->lessThan = &lessThan;
	}


	template<typename T>
	void heapify(T* base, size_t numOfElem)
	{
		for (size_t i1 = parent(numOfElem - 1); i1 < numOfElem; --i1)
		{
			heapify(base, numOfElem, i1);
		}
	}


	// Poped element shall be on &base[numOfElem - 1]
	template<typename T>
	T& pop(T* base, size_t numOfElem)
	{
		if (numOfElem == 0)
		{
			throw domain_error("No more elements in the heap.");
		};

		swap(base[0], base[numOfElem - 1]);
		heapify(base, numOfElem - 1, 0);

		return base[numOfElem - 1];
	}


private :

	static size_t parent(size_t index)
	{
		// "!index" term to make return value 0 when index is 0
		return ( index - 1 + !index ) / numChild;
	}


	static size_t leftmostChild(size_t index)
	{
		return index * numChild + 1;
	}


	template<typename T>
	size_t bestChild(T* base, size_t numOfElem, size_t index)
	{
		size_t best = leftmostChild(index);
		size_t bound = min(best + numChild, numOfElem);
		for (size_t i1 = best + 1; i1 < bound; ++i1)
		{
			bool compareResult;
			if (minHeap)
			{
				compareResult = lessThan->operator()(base[i1], base[best]);
			}
			else
			{
				compareResult = lessThan->operator()(base[best], base[i1]);
			}
			if (compareResult)
			{
				best = i1;
			}
		}

		return best;
	}


	template<typename T>
	void heapify(T* base, size_t numOfElem, size_t index)
	{
		if (index >= numOfElem)
		{
			return;
		}

		T buffer(move(base[index]));
		for (size_t i1, j1 = index; ; )
		{
			i1 = j1;
			j1 = bestChild(base, numOfElem, j1);

			if (j1 >= numOfElem)
			{
				base[i1] = move(buffer);
				break;
			}

			bool compareResult;
			if (minHeap)
			{
				compareResult = lessThan->operator()(base[j1], buffer);
			}
			else
			{
				compareResult = lessThan->operator()(buffer, base[j1]);
			}
			if (compareResult)
			{
				base[i1] = move(base[j1]);
			}
			else
			{
				base[i1] = move(buffer);
				break;
			}
		}
	}
};


template<typename T, typename ComparerType = less<>>
void heapSort(
	T* base,
	size_t numOfElem,
	const ComparerType& lessThan = less<>())
{
	Heapifier<ComparerType> heapifier(lessThan);
	heapifier.heapify(base, numOfElem);
	for (size_t i1 = numOfElem; i1 > 0; --i1)
	{
		heapifier.pop(base, i1);
	}
}


template<typename T, typename ComparerType = less<>>
void sort(T* base, size_t numOfElem, const ComparerType& lessThan = less<>())
{
	sort(base, base + numOfElem, lessThan);
}


template<typename T, typename ComparerType>
struct Sorter
{
	string name;
	void(* fp)(T*, size_t, const ComparerType&);
};


int main(void)
{
	srand((unsigned int)time(nullptr));

	Sorter<int, function<bool(const int&, const int&)>> sorter[] =
	{
		//{ "Built-in", sort },
		//{ "Cocktail Shaker Sort", cocktailShakerSort },
		//{ "Insertion Sort", insertionSort },
		{ "Merge Sort", mergeSort },
		{ "Natural Merge Sort", naturalMergeSort },
		//{ "Heap Sort", heapSort },
		//{ "Quick Sort", quickSort },
	};
	
	size_t unitlen =
#ifdef _DEBUG
		1000
#else
		500000
#endif
		;
	size_t maxlen = 6 * unitlen;
	int* arr[2];
	for (size_t i1 = 0; i1 < ARRLEN(arr); ++i1)
	{
		arr[i1] = new int[maxlen];
	}

	cout << "<Demo>" << endl;
	size_t demoSize = 5;
	for (size_t i1 = 0; i1 < demoSize; ++i1)
	{
		arr[0][i1] = rand();
	}

	for (size_t i1 = 0; i1 < ARRLEN(sorter); ++i1)
	{
		// 각 정렬 알고리듬마다 동일한 배열을 정렬하도록
		memcpy(arr[1], arr[0], sizeof(*arr[1]) * demoSize);
		sorter[i1].fp(arr[1], demoSize, less<>());

		cout << sorter[i1].name << endl << "  ";
		for (size_t i3 = 0; i3 < demoSize; ++i3)
		{
			cout << arr[1][i3] << " ";
		}
		cout << endl;
		cout << "  " << boolalpha << isSorted(arr[1], demoSize) << endl;
	}
	
	auto performSorting = [=](int(*indexMapper)(size_t))
	{
		for (size_t i1 = unitlen; i1 <= maxlen; i1 += unitlen)
		{
			for (size_t i3 = 0; i3 < i1; ++i3)
			{
				arr[0][i3] = indexMapper(i3);
			}

			for (size_t i3 = 0; i3 < ARRLEN(sorter); ++i3)
			{
				// 각 정렬 알고리듬마다 동일한 배열을 정렬하도록
				memcpy(arr[1], arr[0], sizeof(*arr[1]) * i1);
				clock_t start = clock();
				sorter[i3].fp(arr[1], i1, less<>());
				clock_t sortingTime = clock() - start;
				
				if (isSorted(arr[1], i1))
				{
					cout << i1 << " " << sorter[i3].name << " "
						<< sortingTime * 1000LL / CLOCKS_PER_SEC << "ms "
						<< endl;
				}
				else
				{
					cout << "error" << endl;
				}
			}
		}
	};

	auto performExperimentalSorting = [=](int(*indexMapper)(size_t))
	{
		for (size_t i1 = unitlen; i1 <= maxlen; i1 += unitlen)
		{
			for (size_t i3 = 0; i3 < i1; ++i3)
			{
				arr[0][i3] = indexMapper(i3);
			}

			for (size_t i3 = 0; i3 <= 0x100; i3 += 0x10)
			{
				// 각 정렬 알고리듬마다 동일한 배열을 정렬하도록
				memcpy(arr[1], arr[0], sizeof(*arr[1]) * i1);
				clock_t start = clock();
				quickSortPartialFileExperiment(arr[1], i1, i3, less<>());
				clock_t sortingTime = clock() - start;

				if (isSorted(arr[1], i1))
				{
					cout << i1 << " M=" << i3 << " "
						<< sortingTime * 1000LL / CLOCKS_PER_SEC << "ms "
						<< endl;
				}
				else
				{
					cout << "error" << endl;
				}
			}
		}
	};
	
	cout << "<Random number array>" << endl;
	performSorting([](size_t ii) { return rand(); });
	//performExperimentalSorting([](size_t ii) { return rand(); });

	cout << "<Ascending ordered array>" << endl;
	performSorting([](size_t ii) { return (int)ii; });
	//performExperimentalSorting([](size_t ii) { return (int)ii; });

	cout << "<Descending ordered array>" << endl;
	performSorting([](size_t ii) { return -(int)ii; });
	//performExperimentalSorting([](size_t ii) { return -(int)ii; });
	
	for (size_t i1 = 0; i1 < ARRLEN(arr); ++i1)
	{
		delete[] arr[i1];
	}

	return 0;
}

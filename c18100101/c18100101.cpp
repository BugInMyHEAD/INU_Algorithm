#include <iostream>
#include <ctime>
#include <cstdlib>


using namespace std;


const int N = 30000;


inline void swap(int* a, int i, int j)
{
	int t = a[i]; a[i] = a[j]; a[j] = t;
}


void checkSort(int a[], int n)
{
	int i, sorted;
	sorted = true;
	for (i = 1; i < n; i++)
	{
		if (a[i] > a[i + 1]) sorted = false;
		if (!sorted) break;
	}
	if (sorted) cout << "정렬 완료" << endl;
	else cout << "정렬 오류 발생" << endl;
}


void selectionSort(int* a, int N)
{
	int i, j, min;
	for (i = 1; i < N; i++)
	{
		min = i;
		for (j = i + 1; j <= N; j++)
			if (a[j] < a[min]) min = j;
		swap(a, min, i);
	}
}


int main()
{
	int i, a[N + 1];
	double start_time;

	srand((unsigned)time(nullptr));
	for (i = 1; i <= N; i++) a[i] = rand();

	start_time = clock();
	selectionSort(a, N);
	cout << "선택 정렬의 실행시간 (N = "
		<< N
		<< ") : "
		<< clock() - start_time
		<< endl;
	checkSort(a, N);
}
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <array>


#define ARRLEN(x) ( sizeof(x) / sizeof(*(x)) )


using namespace std;


// 비교 결과를 flag로 저장하려 함
typedef int cmp_res_t;
#define CMP_RES_T_NULL 0x00
#define CMP_RES_T_ET 0x01
#define CMP_RES_T_LT 0x02
#define CMP_RES_T_GT 0x04
#define CMP_RES_T_MASK CMP_RES_T_ET | CMP_RES_T_LT | CMP_RES_T_GT
#define CMP_RES_T_LE CMP_RES_T_LT | CMP_RES_T_ET
#define CMP_RES_T_GE CMP_RES_T_GT | CMP_RES_T_ET
#define CMP_RES_T_NE CMP_RES_T_LT | CMP_RES_T_GT
#define CMP_RES_T_ANY CMP_RES_T_ET | CMP_RES_T_LT | CMP_RES_T_GT


// 보통의 비교 함수들이 int 형태로 비교 결과를 반환하기 때문에
// cmp_res_t 형식으로 변환하는 함수
cmp_res_t cmpresi(int zero_to_et_pos_to_gt_neg_to_lt)
{
	switch (
		( zero_to_et_pos_to_gt_neg_to_lt < 0 ) << 1
		| ( zero_to_et_pos_to_gt_neg_to_lt > 0 ) << 0)
	{
	case 0b00:
		return CMP_RES_T_ET;
	case 0b01:
		return CMP_RES_T_GT;
	case 0b10:
		return CMP_RES_T_LT;
	default:
		exit(6);
	}
}


bool isSorted(
	const void* base,
	size_t numOfElem,
	size_t sizeOfElem,
	int(* comparer)(const void*, const void*) )
{
	for (size_t i1 = 1; i1 < numOfElem; ++i1)
	{
		if (
			comparer(
				(const char*)base + (i1 - 1) * sizeOfElem,
				(const char*)base + i1 * sizeOfElem)
			> 0)
		{
			return false;
		}
	}

	return true;
}


// 왼쪽의 수가 더 작으면 -1, 같으면 0, 더 크면 1 반환
int intComparer(const void* a, const void* b)
{
	int aa = *(int*)a;
	int bb = *(int*)b;
	switch ( ( aa < bb ) << 1 | ( aa > bb ) << 0 )
	{
	case 0b00:
		return 0;
	case 0b01:
		return 1;
	case 0b10:
		return -1;
	default:
		exit(6);
	}
}


void cocktailShakerSort(
	int* base,
	size_t numOfElem,
	int(* comparer)(const void*, const void*))
{
	// 진행 방향
	int direction = +1;
	for (
		// left-1과 right 사이가 정렬이 되어있지 않은 부분이다
		size_t left = 1, right = numOfElem - 1;
		left <= right;
		// 1번의 run이 끝날 때마다 진행 방향을 바꿈
		direction = -direction) 
	{
		for (size_t i3 = direction > 0 ? left : right;
			left <= i3 && i3 <= right;
			i3 += direction)
		{
			if (comparer(&base[i3 - 1], &base[i3]) > 0)
			{
				swap(base[i3 - 1], base[i3]);
			}
		}
		// 1번의 run이 끝났을 때의 i3 위치는 정렬이 된 곳이다
		direction > 0 ? --right : ++left;
	}
}


void naturalMergeSort(
	int* base,
	size_t numOfElem,
	int(* comparer)(const void*, const void*))
{
	if (numOfElem <= 1)
	{
		return;
	}

	// 분할 구간을 지정하기 위해 할당
	// 이 배열의 사용 시 인덱스 리스트의 끝에는 두 개의
	// numOfElem이 들어가 있어야 한다
	size_t* partition = new size_t[numOfElem + 2];
	// 합병 시 필요한 배열
	int* spare = new int[numOfElem];

	// 분할 과정
	partition[0] = 0;
	for (size_t i1 = 1, j1 = 0; j1 < numOfElem; ++i1)
	{
		cmp_res_t desirableCompareResult = CMP_RES_T_NULL;
		while (true)
		{
			if (++j1 >= numOfElem)
			{
				partition[i1 + 1] = partition[i1] = numOfElem;
				break;
			}

			cmp_res_t compareResult =
				cmpresi(comparer(&base[j1 - 1], &base[j1]));
			if (desirableCompareResult == CMP_RES_T_NULL)
			{
				desirableCompareResult = compareResult;
			}
			// 오름/내림차순에 변화가 발생하는지 확인한다
			// 두 변수 모두 같은 차순을 의미하는 플래그였다면
			// bitxor에 의해 평가식은 0;
			// 두 변수가 각각 오름차순과 동일을 의미하는 플래그였다면
			// bitand에 의해 평가식은 0
			else if (( desirableCompareResult ^ compareResult ) & CMP_RES_T_GT)
			{
				partition[i1] = j1;
				break;
			}
		}
		// 내림차순이었다면 오름차순으로 바꾼다
		if (desirableCompareResult & CMP_RES_T_GT)
		{
			for (size_t i5 = partition[i1 - 1], j5 = partition[i1] - 1;
				i5 < j5;
				++i5, --j5)
			{
				swap(base[i5], base[j5]);
			}
		}
	}

	// 합병 과정
	// index[0]에는 0이 들어있으므로 ( index[1] >= numOfElem )이면
	// 하나의 구간만 존재한다는 것이므로 모두 합병된 것이다
	while (partition[1] < numOfElem)
	{
		for (size_t i3 = 1, j3 = 1, k3 = 0;
			;
			// 인덱스 리스트의 끝 두 요소가 항상 numOfElem이어야 하는 이유는
			// i3가 2씩 증가하기 때문이다
			i3 += 2, ++j3)
		{
			if (partition[i3 - 1] >= numOfElem)
			{
				// 인덱스 리스트의 끝 두 요소가 항상 numOfElem이 되도록
				partition[j3] = numOfElem;
				break;
			}

			size_t i4 = partition[i3 - 1], j4 = partition[i3];
			while (true)
			{
				// 한 쪽 구간의 요소가 모두 옮겨졌으면
				// 다른 쪽 구간의 남은 요소들을 비교 없이 모두 옮긴다
				if (i4 >= partition[i3])
				{
					while (j4 < partition[i3 + 1])
					{
						spare[k3++] = base[j4++];
					}
					break;
				}
				if (j4 >= partition[i3 + 1])
				{
					while (i4 < partition[i3])
					{
						spare[k3++] = base[i4++];
					}
					break;
				}

				spare[k3++] =
					comparer(&base[i4], &base[j4]) <= 0 ?
						base[i4++] : base[j4++];
			}

			// 합병되었으므로 두 구간을 구별하는 인덱스 i3를 지운다
			// 이 과정으로 인해 구간의 수가 반으로 준다
			// partition[i3 - 1]이 numOfElem이 아니고,
			// partition[i3 + 1]이 numOfElem인
			// 두 가지 경우 모두 partition[j3]에 numOfElem을 대입한다
			// (인덱스 리스트의 끝 두 개는 numOfElem이므로)
			partition[j3] = partition[i3 + 1];
		}

		// 원래의 배열로 복사
		memcpy(base, spare, sizeof(*base) * numOfElem);
	}

	delete[] partition, delete[] spare;
}


int main(void)
{
	struct Sorter
	{
		string name;
		void(*fp)(int*, size_t, int(*)(const void*, const void*));
	};

	srand((unsigned int)time(NULL));

	Sorter sorter[] = {
		{ "Cocktail Shaker Sort", cocktailShakerSort },
		{ "Natural Merge Sort", naturalMergeSort },
	};
	
	size_t maxlen =
#ifdef _DEBUG
		10000;
#else
		50000;
#endif
	int* arr[ARRLEN(sorter)];
	for (size_t i1 = 0; i1 < ARRLEN(arr); ++i1)
	{
		arr[i1] = new int[maxlen];
	}

	cout << "<Demo>" << endl;
	size_t demoSize = 5;
	for (size_t i1 = 0; i1 < demoSize; ++i1)
	{
		arr[0][i1] = rand();
	}
	for (size_t i1 = 1; i1 < ARRLEN(arr); ++i1)
	{
		memcpy(arr[i1], arr[0], sizeof(*arr[i1]) * demoSize);
	}

	for (size_t i1 = 0; i1 < ARRLEN(sorter); ++i1)
	{
		sorter[i1].fp(arr[i1], demoSize, intComparer);

		cout << sorter[i1].name << endl;
		for (size_t i3 = 0; i3 < demoSize; ++i3)
		{
			cout << arr[i1][i3] << " ";
		}
		cout << endl;
	}

	auto performSorting = [=](int(*indexMapper)(size_t))
	{
		for (size_t i1 = 10000; i1 <= maxlen; i1 += 10000)
		{
			for (size_t i3 = 0; i3 < i1; ++i3)
			{
				arr[0][i3] = indexMapper(i3);
			}
			// 각 정렬 알고리듬마다 동일한 배열을 정렬하도록
			for (size_t i3 = 1; i3 < ARRLEN(arr); ++i3)
			{
				memcpy(arr[i3], arr[0], sizeof(*arr[i3]) * i1);
			}

			for (size_t i3 = 0; i3 < ARRLEN(sorter); ++i3)
			{
				clock_t start = clock();
				sorter[i3].fp(arr[i3], i1, intComparer);
				clock_t sortingTime = clock() - start;

				if (isSorted(arr[i3], i1, sizeof(*arr[i3]), intComparer))
				{
					cout << i1 << " " << sorter[i3].name << " "
						<< sortingTime * 1000LL / CLOCKS_PER_SEC << "ms "
						<< endl;
				}
				else
				{
					cout << "error" << endl;
				}
			}
		}
	};
	
	cout << "<Random number array>" << endl;
	performSorting([](size_t ii) { return rand(); });

	cout << "<Ascendant array>" << endl;
	performSorting([](size_t ii) { return (int)ii; });

	cout << "<Descendant array>" << endl;
	performSorting([](size_t ii) { return -(int)ii; });
	
	for (size_t i1 = 0; i1 < ARRLEN(arr); ++i1)
	{
		delete[] arr[i1];
	}

	return 0;
}

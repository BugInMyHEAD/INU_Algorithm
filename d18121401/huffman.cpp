#include <iostream>
#include <cstring>


using namespace std;


class PQ {
private:

	int * heap, * info;
	int n;

public:

	~PQ() { delete[] heap; }

	PQ(int size = 100) {
		heap = new int[size];
		info = new int[size];
		n = 0;
	}

	void insert(int v, int x);
	int remove();
	bool isEmpty() { return n == 0; }
};


void PQ::insert(int v, int x) {
	int i;
	n++;
	for (i = n; ; ) {
		if (i == 1) break;
		if (v >= heap[i / 2]) break;
		heap[i] = heap[i / 2];
		info[i] = info[i / 2];
		i /= 2;
	}
	heap[i] = v;
	info[i] = x;
}


int PQ::remove() {
	int i, j, x, v, temp_v, temp_x;
	x = info[1];
	temp_v = heap[n];
	temp_x = info[n];
	n--;
	i = 1;
	j = 2;
	while (j <= n) {
		if (j < n && heap[j] > heap[j + 1]) j++;
		if (temp_v <= heap[j]) break;
		heap[i] = heap[j];
		info[i] = info[j];
		i = j;
		j *= 2;
	}
	heap[i] = temp_v;
	info[i] = temp_x;
	return x;
}


int index(char c) {
	if (c == 32) return 0;
	else return (c - 64);
}


char ch(int i) {
	return i == 0 ? ' ' : char(i + 64);
}


int main() {
	char text[100] = "VISION QUESTION ONION CAPTION GRADUATION EDUCATION";
	int count[100], dad[100], len[27], code[27];
	PQ pq(100);
	int i, j, k, t, t1, t2, x, M = strlen(text), cnt = 0;

	for (i = 0; i < 100; i++) {
		count[i] = 0;
		dad[i] = 0;
	}
	for (i = 0; i < M; i++) count[index(text[i])]++;

	for (i = 0; i <= 26; i++)
		if (count[i]) pq.insert(count[i], i);
	for (; !pq.isEmpty(); i++) {
		t1 = pq.remove(); t2 = pq.remove();
		dad[i] = 0; dad[t1] = i; dad[t2] = -i;
		count[i] = count[t1] + count[t2];
		if (!pq.isEmpty()) pq.insert(count[i], i);
	}

	for (k = 0; k <= 26; k++) {
		i = 0; x = 0; j = 1;
		if (count[k])
			for (t = dad[k]; t; t = dad[t], j += j, i++)
				if (t < 0) {
					x += j; t = -t;
				}
		code[k] = x; len[k] = i;
	}

	bool encodedResult[300] { 0 };

	for (j = 0; j < M; j++) {
		for (i = len[index(text[j])]; i > 0; i--) {
			encodedResult[cnt] = (code[index(text[j])] >> (i - 1)) & 1;
			std::cout << encodedResult[cnt];
			cnt++;
			if (cnt % 70 == 0) std::cout << std::endl;
		}
	}
	std::cout << std::endl;

	//
	// Homework
	//

	char decodedResult[300] { 0 };

	for (size_t idxEncodedResult = 0, idxDecodedResult = 0;
		idxEncodedResult < cnt;
		) {
		// For all characters
		for (size_t i3 = 0; i3 < std::size(code); ++i3) {
			// If `code[i3]` is valid
			if (len[i3]){
				// While `i7` is less than `len` of `code`
				for (size_t i7 = 0; ; ++i7) {
					if (i7 >= len[i3]) {
						idxEncodedResult += len[i3];
						decodedResult[idxDecodedResult++] = ch(i3);
						break;
					}
					// If the bit sequence of `code`
					// doesn't match `encodedResult`
					if (encodedResult[idxEncodedResult + i7]
						!= ((code[i3] >> (len[i3] - 1 - i7)) & 1)
						) {
						break;
					}
				}
			}
		}
	}
	std::cout << decodedResult << std::endl;
}

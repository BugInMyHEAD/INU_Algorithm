#include <iostream>
#include <ctime>
#include <cstdlib>


using namespace std;


const int N = 10000;

inline void swap(int a[], int i, int j)
{
	int t = a[i];
	a[i] = a[j];
	a[j] = t;
}

void checkSorted(int a[], int n)
{
	int i, Sorted;
	Sorted = true;
	for (i = 1; i < n; i++)
	{
		if (a[i] > a[i + 1])
			Sorted = false;
		if (!Sorted)
			break;
	}
	if (Sorted)
		cout << "정렬 완료." << endl;
	else
		cout << "정렬 오류 발생." << endl;
}

void selectionSort(int a[], int N)
{
	int i, j, min;
	for (i = 1; i < N; i++)
	{
		min = i;
		for (j = i + 1; j <= N; j++)
			if (a[j] < a[min])
				min = j;
		swap(a, min, i);
	}
}

void bubbleSort(int a[], int N)
{
	int i, j;
	for (i = N; i >= 1; i--)
	{
		for (j = 1; j < i; j++)
			if (a[j] > a[j + 1])
				swap(a, j, j + 1);
	}

}

int main()
{
	int i, a[N + 1], b[N + 1], c[N + 1];
	double start_time;

	srand(time(NULL));
	for (i = 1; i <= N; i++)
		a[i] = rand();

	for (i = 1; i <= N; i++)
		b[i] = i;

	for (i = 1; i <= N; i++)
		c[i] = N - i + 1;

	start_time = clock();
	selectionSort(a, N);
	cout << "배열 a[]의 선택 정렬의 실행 시간 (N = " << N << ") : " << clock() - start_time << endl;
	checkSorted(a, N);

	start_time = clock();
	selectionSort(b, N);
	cout << "배열 b[]의 선택 정렬의 실행 시간 (N = " << N << ") : " << clock() - start_time << endl;
	checkSorted(b, N);

	start_time = clock();
	selectionSort(c, N);
	cout << "배열 c[]의 선택 정렬의 실행 시간 (N = " << N << ") : " << clock() - start_time << endl;
	checkSorted(c, N);

	for (i = 1; i <= N; i++)
		a[i] = rand();

	for (i = 1; i <= N; i++)
		b[i] = i;

	for (i = 1; i <= N; i++)
		c[i] = N - i + 1;

	start_time = clock();
	bubbleSort(a, N);
	cout << "배열 a[]의 버블 정렬의 실행 시간 (N = " << N << ") : " << clock() - start_time << endl;
	checkSorted(a, N);

	start_time = clock();
	bubbleSort(b, N);
	cout << "배열 b[]의 버블 정렬의 실행 시간 (N = " << N << ") : " << clock() - start_time << endl;
	checkSorted(b, N);

	start_time = clock();
	bubbleSort(c, N);
	cout << "배열 c[]의 버블 정렬의 실행 시간 (N = " << N << ") : " << clock() - start_time << endl;
	checkSorted(c, N);

	return 0;
}

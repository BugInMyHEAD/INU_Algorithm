#pragma once


#include <stdbool.h>

#include "../../AlgorithmStudying/CDataStructure/BinaryTree.h"


typedef enum EBstError
{
	/**
	 * Failure because of lack of free memory
	 */
	BST_NOMEM = -2,

	/**
	 * Exception because of duplication of a key
	 */
	BST_DUPLICATION,

	/**
	 * It is guaranteed that BST_SUCCESS is 0
	 */
	BST_SUCCESS,
} EBstError;
static_assert(!BST_SUCCESS, "BST_SUCCESS has to be 0");


typedef struct BstNode_postfix BstNode_postfix;
typedef struct Bst_postfix Bst_postfix;

struct BstNode_postfix
{
	BinaryTreeNode_postfix binaryTreeNode;
	size_t numDescendant;
};

struct Bst_postfix
{
	BstNode_postfix* root;
	int(* comparer)(const void*, const void*);
	void(* balancer)(Bst_postfix*, BstNode_postfix*);
};


/**
 * BinarySearchTree 초기화
 */
void Bst_postfix_construct(
	Bst_postfix* bst,
	int(* comparer)(const void*, const void*));

/**
 * BinarySearchTree 소멸
 */
void Bst_postfix_destruct(Bst_postfix* bst);

/**
 *
 *
 * @return HeapData 두 개의 우선순위를 평가하는 함수 포인터
 *         해당 함수의 반환값이 음수이면 첫 번째 인자의 우선순위가 높다고 가정
 *         해당 함수의 반환값이 0이면 두 인자의 우선순위가 동일하다고 가정
 *         해당 함수의 반환값이 양수이면 두 번째 인자의 우선순위가 높다고 가정
 */
int(* Bst_postfix_getComparer(const Bst_postfix* bst))(
	const void*, const void*);

/**
 *
 *
 * @return BinarySearchTree를 구성하는 이진 트리의 루트 노드 주소
 */
/*const BinaryTreeNode* BST_getRoot(const BinarySearchTree* bst);*/

/** 
 * BinarySearchTree에 BinaryTreeData 삽입
 *
 * @return 성공 시 0,
 *         실패 시 음수(절대값이 Heap.c에서의 예외 처리 줄 번호) 반환
 */
EBstError Bst_postfix_insertDup(Bst_postfix* bst, BinaryTreeData data);

/** 
 * BinarySearchTree에 BinaryTreeData 삽입
 *
 * @return 성공 시 0,
 *         실패 시 음수(절대값이 Heap.c에서의 예외 처리 줄 번호) 반환
 */
EBstError Bst_postfix_insertUni(Bst_postfix* bst, BinaryTreeData data);

bool Bst_postfix_search(const Bst_postfix* bst, BinaryTreeData target);

/** 
 * BinarySearchTree에서 특정 BinaryTreeData를 삭제
 *
 * @return 성공 시 0,
 *         실패 시 음수(절대값이 Heap.c에서의 예외 처리 줄 번호) 반환
 */
bool Bst_postfix_delete(
	Bst_postfix* bst, BinaryTreeData target);

/** 
 * BinarySearchTree 순회
 *
 * @param binaryTree_trav BinaryTree.h에 선언된 순회 함수 포인터
 * @param action 순회 시 수행될 행동
 * @return 성공 시 0,
 *         실패 시 음수(절대값이 Heap.c에서의 예외 처리 줄 번호) 반환
 */
size_t Bst_postfix_traverse(
	const Bst_postfix* bst,
	size_t(* binaryTree_trav)(const BinaryTreeNode_postfix*, void(*)(BinaryTreeData)),
	void(* action)(BinaryTreeData));

void noBalance(Bst_postfix* bst, BstNode_postfix* node);
void avlBalance(Bst_postfix* bst, BstNode_postfix* node);
void redBlackBalance(Bst_postfix* bst, BstNode_postfix* node);

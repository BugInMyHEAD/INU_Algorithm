#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#include "../../AlgorithmStudying/customdef.h"

#include "BasicBinarySearchTree.h"

#include "../../AlgorithmStudying/CDataStructure/BinaryTree.c"


int(* Bst_postfix_getComparer(const Bst_postfix* bst))(
	const void*, const void*)
{
	return bst->comparer;
}


static size_t BstNode_postfix_getNumDescendant(BstNode_postfix* node)
{
	return node->numDescendant;
}


static size_t BstNode_postfix_getSize(BstNode_postfix* node)
{
	return node ? BstNode_postfix_getNumDescendant(node) + 1 : 0;
}


static size_t BstNode_postfix_getWeight(BstNode_postfix* node)
{
	return BstNode_postfix_getSize(node) + 1;
}


void Bst_postfix_construct(
	Bst_postfix* bst,
	int(* comparer)(const void*, const void*))
{
	bst->root = NULL;
	bst->comparer = comparer;
}


static BstNode_postfix* Bst_postfix_makeNode(void)
{
	BstNode_postfix* node;
	if (!ASSIGN_TOTAL(malloc, node, 1))
	{
		return NULL;
	}

	BinaryTree_postfix_initNode(&node->binaryTreeNode);
	node->numDescendant = 0;

	return node;
}


static void Bst_postfix_destroyNode(BstNode_postfix* node)
{
	free(node);
}


/**
 *
 *
 * @return node에 저장되어 있는 BinaryTreeData
 */
static BinaryTreeData BstNode_postfix_getData(const BstNode_postfix* node)
{
	return BinaryTree_postfix_getData(&node->binaryTreeNode);
}


/**
 *
 *
 * @return node에 저장되어 있는 BinaryTreeData
 */
static BinaryTreeData* BstNode_postfix_getDataPtr(BstNode_postfix* node)
{
	return BinaryTree_postfix_getDataPtr(&node->binaryTreeNode);
}


/**
 * node에 저장되어 있는 BinaryTreeData를 교체
 */
void BstNode_postfix_setData(BstNode_postfix* node, BinaryTreeData data)
{
	BinaryTree_postfix_setData(&node->binaryTreeNode, data);
}


/**
 *
 *
 * @return 왼쪽 자식 노드의 주소
 */
static BstNode_postfix* BstNode_postfix_getLeft(const BstNode_postfix* node)
{
	BinaryTreeNode_postfix* binaryTreeNodePointer =
		BinaryTree_postfix_getLeft(&node->binaryTreeNode);
	return WHOSE(BstNode_postfix, binaryTreeNode, binaryTreeNodePointer);
}


/**
 *
 *
 * @return 오른쪽 자식 노드의 주소
 */
static BstNode_postfix* BstNode_postfix_getRight(const BstNode_postfix* node)
{
	BinaryTreeNode_postfix* binaryTreeNodePointer =
		BinaryTree_postfix_getRight(&node->binaryTreeNode);
	return WHOSE(BstNode_postfix, binaryTreeNode, binaryTreeNodePointer);
}


/**
 *
 *
 * @return 부모 노드의 주소
 */
static BstNode_postfix* BstNode_postfix_getParent(const BstNode_postfix* node)
{
	BinaryTreeNode_postfix* binaryTreeNodePointer =
		BinaryTree_postfix_getParent(&node->binaryTreeNode);
	return WHOSE(BstNode_postfix, binaryTreeNode, binaryTreeNodePointer);
}


/**
 *
 *
 * @return 가장 왼쪽에 있는 노드의 주소(node 포함)
 */
static BstNode_postfix* BstNode_postfix_getLeftmost(BstNode_postfix* node)
{
	BinaryTreeNode_postfix* binaryTreeNodePointer =
		BinaryTree_postfix_getLeftmost(&node->binaryTreeNode);
	return WHOSE(BstNode_postfix, binaryTreeNode, binaryTreeNodePointer);
}


/**
 *
 *
 * @return 가장 오른쪽에 있는 노드의 주소(node 포함)
 */
static BstNode_postfix* BstNode_postfix_getRightmost(BstNode_postfix* node)
{
	BinaryTreeNode_postfix* binaryTreeNodePointer =
		BinaryTree_postfix_getRightmost(&node->binaryTreeNode);
	return WHOSE(BstNode_postfix, binaryTreeNode, binaryTreeNodePointer);
}


/**
 * 왼쪽 서브 트리 교체
 *
 * @param parent NULL이면 child가 루트 노드가 됨
 * @param child NULL이면 parent의 왼쪽 자식 노드가 NULL이 됨
 * @return 분리된 트리의 루트 노드의 주소
 */
static BstNode_postfix* BstNode_postfix_changeLeft(
	BstNode_postfix* parent, BstNode_postfix* child)
{
	BinaryTreeNode_postfix* binaryTreeNodePointer =
		BinaryTree_postfix_changeLeft(
			&parent->binaryTreeNode, &child->binaryTreeNode);
	return WHOSE(BstNode_postfix, binaryTreeNode, binaryTreeNodePointer);
}


/**
 * 오른쪽 서브 트리 교체
 *
 * @param parent NULL이면 child가 루트 노드가 됨
 * @param child NULL이면 parent의 오른쪽 자식 노드가 NULL이 됨
 * @return 분리된 트리의 루트 노드의 주소
 */
static BstNode_postfix* BstNode_postfix_changeRight(
	BstNode_postfix* parent, BstNode_postfix* child)
{
	BinaryTreeNode_postfix* binaryTreeNodePointer =
		BinaryTree_postfix_changeRight(
			&parent->binaryTreeNode, &child->binaryTreeNode);
	return WHOSE(BstNode_postfix, binaryTreeNode, binaryTreeNodePointer);
}


static EBinaryTreePosition BstNode_postfix_getPosition(
	const BstNode_postfix* node)
{
	return BinaryTree_postfix_getPosition(&node->binaryTreeNode);
}


//static void BstNode_postfix_swap(BstNode_postfix* a, BstNode_postfix* b)
//{
//	SWAP(a->numDescendant, b->numDescendant);
//	BinaryTree_postfix_swap(&a->binaryTreeNode, &b->binaryTreeNode);
//}


/**
 *
 *
 * @return 루트 노드이면 1, 그렇지 않으면 0
 */
static bool BstNode_postfix_isRoot(const BstNode_postfix* node)
{
	return BinaryTree_postfix_isRoot(&node->binaryTreeNode);
}


/**
 *
 *
 * @return 리프 노드이면 1, 그렇지 않으면 0
 */
static bool BstNode_postfix_isLeaf(const BstNode_postfix* node)
{
	return BinaryTree_postfix_isLeaf(&node->binaryTreeNode);
}


static void Bst_postfix_destructRecur(BstNode_postfix* node)
{
	if (!node)
	{
		return;
	}

	Bst_postfix_destructRecur(BstNode_postfix_getLeft(node));
	Bst_postfix_destructRecur(BstNode_postfix_getRight(node));
	Bst_postfix_destroyNode(node);
}


void Bst_postfix_destruct(Bst_postfix* bst)
{
	Bst_postfix_destructRecur(bst->root);
	bst->root = NULL;
}


static BstNode_postfix* BstNode_postfix_search(
	const Bst_postfix* bst, BinaryTreeData target)
{
	int(* comparer)(const void*, const void*) = Bst_postfix_getComparer(bst);

	/* target에 대한 이진 트리 탐색 */
	BstNode_postfix* result;
	result = bst->root;
	while (1)
	{
		/* target이 존재하지 않는 경우 */
		if(!result)
		{
			return NULL;
		}

		int compareResult = comparer(&target, &result->binaryTreeNode.data);

		/* target을 찾았음 */
		if (compareResult == 0)
		{
			break;
		}

		result =
			( compareResult < 0 ?
				BstNode_postfix_getLeft :
				BstNode_postfix_getRight )(result);
	}

	return result;
}


bool Bst_postfix_search(
	const Bst_postfix* bst, BinaryTreeData target)
{
	return BstNode_postfix_search(bst, target);
}


static EBstError Bst_postfix_insert(
	Bst_postfix* bst, BinaryTreeData data, bool uni)
{
	int(* comparer)(const void*, const void*) = Bst_postfix_getComparer(bst);

	BstNode_postfix* parent, * child;
	int compareResult;

	/* 새로운 노드가 추가될 위치를 찾음 */
	parent = NULL;
	child = bst->root;
	while (child)
	{
		compareResult = comparer(&data, BstNode_postfix_getDataPtr(child));

		if (compareResult == 0 && uni)
		{
			return BST_DUPLICATION;
		}

		assert(BstNode_postfix_getPosition(child));

		parent = child;
		child =
			( compareResult < 0 ?
				BstNode_postfix_getLeft :
				BstNode_postfix_getRight )(child);
	}

	/* 새 노드 동적 할당 */
	child = Bst_postfix_makeNode();
	if (!child)
	{
		return BST_NOMEM;
	}
	BstNode_postfix_setData(child, data);

	/* 새 노드가 루트가 아니면 */
	if (parent)
	{
		( compareResult < 0 ?
			BstNode_postfix_changeLeft :
			BstNode_postfix_changeRight )(parent, child);
	}
	/* 새 노드가 루트이면 */
	else
	{
		bst->root = child;
	}

	assert(BstNode_postfix_getPosition(child));

	bst->balancer(bst, child);

	assert(BstNode_postfix_getPosition(child));
	assert(BstNode_postfix_getPosition(BstNode_postfix_getLeft(child)));
	assert(BstNode_postfix_getPosition(BstNode_postfix_getRight(child)));

	return BST_SUCCESS;
}


EBstError Bst_postfix_insertDup(Bst_postfix* bst, BinaryTreeData data)
{
	return Bst_postfix_insert(bst, data, false);
}


EBstError Bst_postfix_insertUni(Bst_postfix* bst, BinaryTreeData data)
{
	return Bst_postfix_insert(bst, data, true);
}


bool Bst_postfix_delete(
	Bst_postfix* bst, BinaryTreeData target)
{
	int(* comparer)(const void*, const void*) = Bst_postfix_getComparer(bst);

	/* target에 대한 이진 트리 탐색 */
	BstNode_postfix* deleted;
	deleted = bst->root;
	while (1)
	{
		/* target이 존재하지 않는 경우 */
		if(!deleted)
		{
			return false;
		}

		int compareResult = comparer(&target, &deleted->binaryTreeNode.data);

		/* target을 찾았음 */
		if (compareResult == 0)
		{
			break;
		}

		deleted =
			( compareResult < 0 ?
				BstNode_postfix_getLeft :
				BstNode_postfix_getRight )(deleted);
	}

	/* result의 오른쪽 서브트리 중 가장 왼쪽에 있는 노드 */
	BstNode_postfix* promoted =
		BstNode_postfix_getLeftmost(BstNode_postfix_getRight(deleted));
	BstNode_postfix* parent;
	/* result의 오른쪽 서브트리가 있으면 */
	if (promoted)
	{
		/* promoted는 자신의 오른쪽 서브트리를 부모에 붙여놓고 분리됨 */
		parent = BstNode_postfix_getParent(promoted);
		BstNode_postfix* right = BstNode_postfix_getRight(promoted);
		( parent == deleted ?
			BstNode_postfix_changeRight :
			BstNode_postfix_changeLeft )(parent, right);
		/* promoted가 delete를 대체하면서 result가 분리됨 */
		deleted->binaryTreeNode.data = promoted->binaryTreeNode.data;
		deleted = promoted;
	}
	/* result의 오른쪽 서브트리가 없으면 */
	else
	{
		parent = BstNode_postfix_getParent(deleted);
		BstNode_postfix* left = BstNode_postfix_getLeft(deleted);
		if (BstNode_postfix_getLeft(parent) == deleted)
		{
			BstNode_postfix_changeLeft(parent, left);
		}
		if (BstNode_postfix_getRight(parent) == deleted)
		{
			BstNode_postfix_changeRight(parent, left);
		}
	}

	bst->balancer(bst, parent);

	return true;
}


size_t Bst_postfix_traverse(
	const Bst_postfix* bst,
	size_t(* binaryTree_trav)(const BinaryTreeNode_postfix*, void(*)(BinaryTreeData)),
	void(* action)(BinaryTreeData))
{
	return binaryTree_trav(&bst->root->binaryTreeNode, action);
}


static void BstNode_postfix_seesaw(
	Bst_postfix* bst, BstNode_postfix* node)
{
	if (!node)
	{
		return;
	}
	BstNode_postfix* parent = BstNode_postfix_getParent(node);
	if (!parent)
	{
		return;
	}

	EBinaryTreePosition parentPosition = BstNode_postfix_getPosition(parent);
	EBinaryTreePosition nodePosition = BstNode_postfix_getPosition(node);
	BstNode_postfix* grandp = BstNode_postfix_getParent(parent);
	switch (parentPosition)
	{
	case BINARY_TREE_LEFT :
		BstNode_postfix_changeLeft(grandp, node);
		break;
	case BINARY_TREE_RIGHT :
		BstNode_postfix_changeRight(grandp, node);
		break;
	case BINARY_TREE_NO_PARENT :
		assert(!grandp);
		assert(BstNode_postfix_isRoot(parent));
		bst->root = node;
		BstNode_postfix_changeLeft(NULL, node);
		break;
	default :
		exit(EXIT_FAILURE);
	}

	assert(!BstNode_postfix_getParent(parent));
	assert(BstNode_postfix_getLeft(parent) != node);
	assert(BstNode_postfix_getLeft(node) != parent);
	assert(BstNode_postfix_getRight(parent) != node);
	assert(BstNode_postfix_getRight(node) != parent);
	assert(BstNode_postfix_getParent(node) == grandp);
	switch (nodePosition)
	{
	case BINARY_TREE_LEFT :
		BstNode_postfix_changeLeft(
			parent, BstNode_postfix_changeRight(node, parent));
		break;
	case BINARY_TREE_RIGHT :
		BstNode_postfix_changeRight(
			parent, BstNode_postfix_changeLeft(node, parent));
		break;
	default :
		exit(EXIT_FAILURE);
	}

	assert(BstNode_postfix_getPosition(parent));
	assert(BstNode_postfix_getPosition(BstNode_postfix_getLeft(parent)));
	assert(BstNode_postfix_getPosition(BstNode_postfix_getRight(parent)));
	assert(BstNode_postfix_getPosition(node));
	assert(BstNode_postfix_getPosition(grandp));
}


/**
 *	@return The new parent node among involved three nodes
 */
static BstNode_postfix* BstNode_postfix_rotate(
	Bst_postfix* bst, BstNode_postfix* node)
{
	if (!node)
	{
		return NULL;
	}
	BstNode_postfix* parent = BstNode_postfix_getParent(node);
	if (!parent)
	{
		return NULL;
	}
	BstNode_postfix* grandp = BstNode_postfix_getParent(parent);
	if (!grandp)
	{
		return NULL;
	}

	assert(BstNode_postfix_getPosition(node) == BINARY_TREE_LEFT
		|| BstNode_postfix_getPosition(node) == BINARY_TREE_RIGHT);
	assert(BstNode_postfix_getPosition(parent) == BINARY_TREE_LEFT
		|| BstNode_postfix_getPosition(parent) == BINARY_TREE_RIGHT);

	BstNode_postfix* newParent;
	if (BstNode_postfix_getPosition(node)
		== BstNode_postfix_getPosition(parent))
	{
		// Single rotation
		// parent has the middle value
		newParent = parent;
	}
	else
	{
		// Double rotation
		// node has the middle value
		BstNode_postfix_seesaw(bst, node);
		newParent = node;
		
		assert(BstNode_postfix_getPosition(parent) == BINARY_TREE_LEFT
			|| BstNode_postfix_getPosition(parent) == BINARY_TREE_RIGHT);
	}
	BstNode_postfix_seesaw(bst, newParent);

	assert(BstNode_postfix_getPosition(grandp) == BINARY_TREE_LEFT
		|| BstNode_postfix_getPosition(grandp) == BINARY_TREE_RIGHT);
	assert(BstNode_postfix_getPosition(node));
	assert(BstNode_postfix_getPosition(newParent));
	assert(BstNode_postfix_getPosition(BstNode_postfix_getLeft(newParent)) == BINARY_TREE_LEFT);
	assert(BstNode_postfix_getPosition(BstNode_postfix_getRight(newParent)) == BINARY_TREE_RIGHT);

	return newParent;
}


static BstNode_postfix* BstNode_postfix_getUncle(BstNode_postfix* child)
{
	if (!child)
	{
		return NULL;
	}
	BstNode_postfix* parent = BstNode_postfix_getParent(child);
	if (!parent)
	{
		return NULL;
	}
	BstNode_postfix* grandp = BstNode_postfix_getParent(parent);
	if (!grandp)
	{
		return NULL;
	}

	switch (BstNode_postfix_getPosition(parent))
	{
	case BINARY_TREE_LEFT :
		return BstNode_postfix_getRight(grandp);
	case BINARY_TREE_RIGHT :
		return BstNode_postfix_getLeft(grandp);
	default :
		exit(EXIT_FAILURE);
	}
}


void noBalance(Bst_postfix* bst, BstNode_postfix* node)
{
	// Do nothing
}


void avlBalance(Bst_postfix* bst, BstNode_postfix* node)
{
	BstNode_postfix* child = node;
	BstNode_postfix* parent = BstNode_postfix_getParent(node);
	BstNode_postfix* grandp = BstNode_postfix_getParent(parent);

	EBinaryTreePosition posChild = BstNode_postfix_getPosition(child);
	switch (posChild)
	{
	case BINARY_TREE_LEFT :
		--parent->numDescendant;
		break;
	case BINARY_TREE_RIGHT :
		++parent->numDescendant;
		break;
	case BINARY_TREE_NO_PARENT :
		return;
	default :
		exit(EXIT_FAILURE);
	}

	while (parent->numDescendant)
	{
		EBinaryTreePosition posParent = BstNode_postfix_getPosition(parent);
		switch (posParent)
		{
		case BINARY_TREE_LEFT :
			--grandp->numDescendant;
			break;
		case BINARY_TREE_RIGHT :
			++grandp->numDescendant;
			break;
		case BINARY_TREE_NO_PARENT :
			return;
		default:
			exit(EXIT_FAILURE);
		}
		assert(parent->numDescendant == -1 || parent->numDescendant == 0 || parent->numDescendant == 1);
		if (
			!(
				grandp->numDescendant == -1
				|| grandp->numDescendant == 0
				|| grandp->numDescendant == 1))
		{
			parent->numDescendant = grandp->numDescendant = 0;
			parent = BstNode_postfix_rotate(bst, child);

			assert(BstNode_postfix_getPosition(parent));
			assert(BstNode_postfix_getPosition(BstNode_postfix_getLeft(parent)) == BINARY_TREE_LEFT);
			assert(BstNode_postfix_getPosition(BstNode_postfix_getRight(parent)) == BINARY_TREE_RIGHT);

			break;
		}

		assert(BstNode_postfix_getPosition(parent));
		assert(BstNode_postfix_getPosition(BstNode_postfix_getLeft(parent)));
		assert(BstNode_postfix_getPosition(BstNode_postfix_getRight(parent)));

		// Going up
		child = parent;
		parent = grandp;
		grandp = BstNode_postfix_getParent(grandp);
	}
}


void redBlackBalance(Bst_postfix* bst, BstNode_postfix* node)
{
	// Turn the node red
	node->numDescendant = 1;

	BstNode_postfix* child = node;
	BstNode_postfix* parent = BstNode_postfix_getParent(node);
	BstNode_postfix* grandp = BstNode_postfix_getParent(parent);
	while (grandp)
	{
		if (child->numDescendant && parent->numDescendant)
		{
			BstNode_postfix* uncle = BstNode_postfix_getUncle(child);
			if (uncle && uncle->numDescendant)
			{
				parent->numDescendant = uncle->numDescendant = 0;
				grandp->numDescendant = 1;
			}
			else
			{
				parent = BstNode_postfix_rotate(bst, child);
				grandp = BstNode_postfix_getParent(parent);
				parent->numDescendant = 0;
				BstNode_postfix_getLeft(parent)->numDescendant = 1;
				BstNode_postfix_getRight(parent)->numDescendant = 1;

				assert(BstNode_postfix_getPosition(parent));
				assert(BstNode_postfix_getPosition(BstNode_postfix_getLeft(parent)) == BINARY_TREE_LEFT);
				assert(BstNode_postfix_getPosition(BstNode_postfix_getRight(parent)) == BINARY_TREE_RIGHT);
			}
		}

		assert(BstNode_postfix_getPosition(parent));
		assert(BstNode_postfix_getPosition(BstNode_postfix_getLeft(parent)));
		assert(BstNode_postfix_getPosition(BstNode_postfix_getRight(parent)));

		// Going up
		child = parent;
		parent = grandp;
		grandp = BstNode_postfix_getParent(grandp);
	}

	// Always make the root node black
	bst->root->numDescendant = 0;
}
// 인천대학교 컴퓨터공학부
// 2018년 2학기 채진석 교수님 알고리즘
//
// 프로그래밍 과제 #3


#define _USE_MATH_DEFINES

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "!declaration.h"

#include "BasicBinarySearchTree.h"


int customStrcmp(const void* a, const void* b)
{
	return strcmp(*(const char**)a, *(const char**)b);
}


FILE* testFile = NULL;


void testPrepare(void)
{
	errno_t err;
	err = fopen_s(&testFile, "test.txt", "wb");
	if (err)
	{
		fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
		exit(EXIT_FAILURE);
	}
}


void testWrite(const void* buf)
{
	fputs(buf, testFile);
}


void testDispose(void)
{
	fclose(testFile);
	testFile = NULL;
}


int main(void)
{
	srand((unsigned int)time(NULL));

	FILE* file;
	errno_t err;
	clock_t start;
	const int sampleQty = 8;
	const int slowAlgSampleQty = 1;

	LList_char_ptr_t dictList, searchList;
	LList_char_ptr_t_init(&dictList);
	LList_char_ptr_t_init(&searchList);
	LList_char_ptr_t_Iter dictIter, searchIter;
	LList_char_ptr_t_iterHead(&dictList, &dictIter);
	LList_char_ptr_t_iterHead(&searchList, &searchIter);

	Bst_postfix basic, avl, redBlack;
	Bst_postfix_construct(&basic, customStrcmp);
	Bst_postfix_construct(&avl, customStrcmp);
	Bst_postfix_construct(&redBlack, customStrcmp);
	basic.balancer = noBalance;
	avl.balancer = avlBalance;
	redBlack.balancer = redBlackBalance;

	// 파일 읽어서 리스트에 저장(메모리에 저장)

	err = fopen_s(&file, "search_key.txt", "r");
	if (err)
	{
		fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
		exit(EXIT_FAILURE);
	}
	while (true)
	{
		char* buf;
		if (!ASSIGN_TOTAL(malloc, buf, 11))
		{
			fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
			exit(EXIT_FAILURE);
		}
		if (!fgets(buf, 11, file))
		{
			free(buf);
			break;
		}
		if (LList_char_ptr_t_addLast(&searchIter, buf))
		{
			fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
			exit(EXIT_FAILURE);
		}
	}
	fclose(file);

	err = fopen_s(&file, "dict.txt", "r");
	if (err)
	{
		fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
		exit(EXIT_FAILURE);
	}
	while (true)
	{
		char* buf;
		if (!ASSIGN_TOTAL(malloc, buf, 11))
		{
			fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
			exit(EXIT_FAILURE);
		}
		if (!fgets(buf, 11, file))
		{
			free(buf);
			break;
		}
		if (LList_char_ptr_t_addLast(&dictIter, buf))
		{
			fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
			exit(EXIT_FAILURE);
		}
	}
	fclose(file);

	printf("Inserting <dict.txt>...\n");

	// 기본 BST에 정렬되지 않은 레코드 삽입

	LList_char_ptr_t_goHead(&dictIter);
	start = clock();
	for (int i1 = 1; i1 <= sampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
				(double)LList_char_ptr_t_getSize(&dictList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&dictIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&dictIter);
			char* buf = LList_char_ptr_t_getData(&dictIter);
			if (Bst_postfix_insertUni(&basic, buf))
			{
				fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
				exit(EXIT_FAILURE);
			}
		}
		printf("%6zd Basic BST: %d\n", i2, clock() - start);
	}

	// AVL 트리에 정렬되지 않은 레코드 삽입

	LList_char_ptr_t_goHead(&dictIter);
	start = clock();
	for (int i1 = 1; i1 <= sampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
			(double)LList_char_ptr_t_getSize(&dictList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&dictIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&dictIter);
			char* buf = LList_char_ptr_t_getData(&dictIter);
			if (Bst_postfix_insertUni(&avl, buf))
			{
				fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
				exit(EXIT_FAILURE);
			}
		}
		printf("%6zd AVL tree: %d\n", i2, clock() - start);
	}

	// 레드블랙 트리에 정렬되지 않은 레코드 삽입

	LList_char_ptr_t_goHead(&dictIter);
	start = clock();
	for (int i1 = 1; i1 <= sampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
				(double)LList_char_ptr_t_getSize(&dictList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&dictIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&dictIter);
			char* buf = LList_char_ptr_t_getData(&dictIter);
			if (Bst_postfix_insertUni(&redBlack, buf))
			{
				fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
				exit(EXIT_FAILURE);
			}
		}
		printf("%6zd Red-black tree: %d\n", i2, clock() - start);
	}

	puts("");

	printf("Searching...\n");

	// 정렬되지 않은 레코드를 넣은 기본 BST에서 탐색

	LList_char_ptr_t_goHead(&searchIter);
	start = clock();
	for (int i1 = 1; i1 <= sampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
				(double)LList_char_ptr_t_getSize(&searchList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&searchIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&searchIter);
			char* buf = LList_char_ptr_t_getData(&searchIter);
			if (!Bst_postfix_search(&basic, buf))
			{
				printf("??\n");
				exit(EXIT_SUCCESS);
			}
		}
		printf("%6zd Basic BST: %d\n", i2, clock() - start);
	}

	// 정렬되지 않은 레코드를 넣은 AVL 트리에서 탐색
	
	LList_char_ptr_t_goHead(&searchIter);
	start = clock();
	for (int i1 = 1; i1 <= sampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
			(double)LList_char_ptr_t_getSize(&searchList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&searchIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&searchIter);
			char* buf = LList_char_ptr_t_getData(&searchIter);
			if (!Bst_postfix_search(&avl, buf))
			{
				printf("??\n");
				exit(EXIT_SUCCESS);
			}
		}
		printf("%6zd AVL tree: %d\n", i2, clock() - start);
	}

	// 정렬되지 않은 레코드를 넣은 레드블랙 트리에서 탐색

	LList_char_ptr_t_goHead(&searchIter);
	start = clock();
	for (int i1 = 1; i1 <= sampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
				(double)LList_char_ptr_t_getSize(&searchList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&searchIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&searchIter);
			char* buf = LList_char_ptr_t_getData(&searchIter);
			if (!Bst_postfix_search(&redBlack, buf))
			{
				printf("??\n");
				exit(EXIT_SUCCESS);
			}
		}
		printf("%6zd Red-black tree: %d\n", i2, clock() - start);
	}

	puts("");

	// 기존에 삽입된 레코드 삭제

	Bst_postfix_destruct(&basic);
	Bst_postfix_destruct(&avl);
	Bst_postfix_destruct(&redBlack);

	while (LList_char_ptr_t_goFirst(&dictIter))
	{
		char* buf;
		LList_char_ptr_t_removeGoNext(&dictIter, &buf);
		free(buf);
	}

	// 파일 읽어서 리스트에 저장(메모리에 저장)

	err = fopen_s(&file, "sorted_dict.txt", "r");
	if (err)
	{
		exit(EXIT_FAILURE);
	}
	while (true)
	{
		char* buf;
		if (!ASSIGN_TOTAL(malloc, buf, 11))
		{
			exit(EXIT_FAILURE);
		}
		if (!fgets(buf, 11, file))
		{
			free(buf);
			break;
		}
		if (LList_char_ptr_t_addLast(&dictIter, buf))
		{
			exit(EXIT_FAILURE);
		}
	}
	fclose(file);

	printf("Inserting <sorted_dict.txt>...\n");

	// 기본 BST에 정렬된 레코드 삽입

	LList_char_ptr_t_goHead(&dictIter);
	start = clock();
	// 시간이 너무 오래 걸려서 임의로 샘플 수를 줄임
	for (int i1 = 1; i1 <= slowAlgSampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
				(double)LList_char_ptr_t_getSize(&dictList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&dictIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&dictIter);
			char* buf = LList_char_ptr_t_getData(&dictIter);
			if (Bst_postfix_insertUni(&basic, buf))
			{
				fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
				exit(EXIT_FAILURE);
			}
		}
		printf("%6zd Basic BST: %d\n", i2, clock() - start);
	}

	// AVL 트리에 정렬된 레코드 삽입

	LList_char_ptr_t_goHead(&dictIter);
	start = clock();
	for (int i1 = 1; i1 <= sampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
			(double)LList_char_ptr_t_getSize(&dictList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&dictIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&dictIter);
			char* buf = LList_char_ptr_t_getData(&dictIter);
			if (Bst_postfix_insertUni(&avl, buf))
			{
				fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
				exit(EXIT_FAILURE);
			}
		}
		printf("%6zd avl tree: %d\n", i2, clock() - start);
	}

	// 레드블랙 트리에 정렬된 레코드 삽입

	LList_char_ptr_t_goHead(&dictIter);
	start = clock();
	for (int i1 = 1; i1 <= sampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
				(double)LList_char_ptr_t_getSize(&dictList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&dictIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&dictIter);
			char* buf = LList_char_ptr_t_getData(&dictIter);
			if (Bst_postfix_insertUni(&redBlack, buf))
			{
				fprintf(stderr, "Error occured. Line:%d\n", __LINE__);
				exit(EXIT_FAILURE);
			}
		}
		printf("%6zd Red-black tree: %d\n", i2, clock() - start);
	}

	puts("");

	printf("Searching...\n");

	// 정렬된 레코드를 넣은 기본 BST에서 탐색

	LList_char_ptr_t_goHead(&searchIter);
	start = clock();
	for (int i1 = 1; i1 <= slowAlgSampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
				(double)LList_char_ptr_t_getSize(&searchList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&searchIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&searchIter);
			char* buf = LList_char_ptr_t_getData(&searchIter);
			if (!Bst_postfix_search(&basic, buf))
			{
				// 임의로 샘플 수를 줄였으므로 모든 키가 존재하지는 않음
			}
		}
		printf("%6zd Basic BST: %d\n", i2, clock() - start);
	}

	// 정렬된 레코드를 넣은 AVL 트리에서 탐색

	LList_char_ptr_t_goHead(&searchIter);
	start = clock();
	for (int i1 = 1; i1 <= sampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
			(double)LList_char_ptr_t_getSize(&searchList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&searchIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&searchIter);
			char* buf = LList_char_ptr_t_getData(&searchIter);
			if (!Bst_postfix_search(&avl, buf))
			{
				printf("??\n");
				exit(EXIT_SUCCESS);
			}
		}
		printf("%6zd AVL tree: %d\n", i2, clock() - start);
	}

	// 정렬된 레코드를 넣은 레드블랙 트리에서 탐색

	LList_char_ptr_t_goHead(&searchIter);
	start = clock();
	for (int i1 = 1; i1 <= sampleQty; ++i1)
	{
		size_t i2 =
			(size_t)(
				(double)LList_char_ptr_t_getSize(&searchList) * i1 / sampleQty);
		while (LList_char_ptr_t_getIndex(&searchIter) != i2 - LLIST_NUM_DUMMY)
		{
			LList_char_ptr_t_goNext(&searchIter);
			char* buf = LList_char_ptr_t_getData(&searchIter);
			if (!Bst_postfix_search(&redBlack, buf))
			{
				printf("??\n");
				exit(EXIT_SUCCESS);
			}
		}
		printf("%6zd Red-black tree: %d\n", i2, clock() - start);
	}

	puts("");

	// 테스트용 코드

	testPrepare();
	Bst_postfix_traverse(&avl, BinaryTree_postfix_travIn, testWrite);
	testDispose();

	// 메모리 정리

	while (LList_char_ptr_t_goFirst(&dictIter))
	{
		char* buf;
		LList_char_ptr_t_removeGoPrev(&dictIter, &buf);
		free(buf);
	}
	while (LList_char_ptr_t_goFirst(&searchIter))
	{
		char* buf;
		LList_char_ptr_t_removeGoNext(&searchIter, &buf);
		free(buf);
	}
	LList_char_ptr_t_iterUnref(&dictIter);
	LList_char_ptr_t_iterUnref(&searchIter);

	printf("Complete\n");

	return 0;
}
